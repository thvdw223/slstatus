# slstatus - suckless status

[slstatus][suckless-slstatus] is a suckless status monitor for
window managers that use `WM_NAME` (e.g. dwm) or stdin to fill
the status bar.

## Branches

- `source`:
   - Files are from suckless except `.gitignore` and `README.md`.
   - The _source_ branch should **not be patched or customized**.
- `patch/RELEASE/feature`:
   - A _patch_ branch should be created from the _source_ branch.
   - Each feature should be patched on an **individual branch**.
   - The _RELEASE_ should correspond to the slstatus version.
- `platform/RELEASE/os`:
   - A _platform_ branch should be created from the _source_ branch.
   - The _platform_ branch should **not be patched**.
   - The _RELEASE_ should correspond to the slstatus version.
- `build/RELEASE/os`:
   - A _build_ branch should be created from the _source_ branch.
   - Merge the required feature or platform branches into a _build_ branch.
   - Customization should be **after merging branches**.
   - The _RELEASE_ should correspond to the slstatus version.

## Features

- Battery percentage/state/time left
- CPU usage
- CPU frequency
- Custom shell commands
- Date and time
- Disk status (free storage, percentage, total storage and used storage)
- Available entropy
- Username/GID/UID
- Hostname
- IP address (IPv4 and IPv6)
- Kernel version
- Keyboard indicators
- Keymap
- Load average
- Network speeds (RX and TX)
- Number of files in a directory (hint: Maildir)
- Memory status (free memory, percentage, total memory and used memory)
- Swap status (free swap, percentage, total swap and used swap)
- Temperature
- Uptime
- Volume percentage
- WiFi signal percentage and ESSID

## Requirements

In order to build slstatus you need the Xlib header files.

## Installation

Edit `config.mk` to match your local setup (slstatus is installed into the
`/usr/local` namespace by default).

Afterwards enter the following command to build and install slstatus (if
necessary as root):

```shell
$ make clean install
```

## Running slstatus

Add the following line to your `.xinitrc` or other startup script:

```
slstatus &
```

See the man page for details.

## Configuration

slstatus can be customized by creating a custom `config.h` and (re)compiling
the source code. This keeps it fast, secure and simple.

## Credits

- [Suckless project][suckless-website]

[suckless-slstatus]: https://tools.suckless.org/slstatus/
[suckless-website]: https://suckless.org/
